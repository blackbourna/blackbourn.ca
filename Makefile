default:
	@echo 'cc: clear cache'
	@echo 'cc-prod: clear prod cache'
	@echo 'clean: delete cache and vendor dirs'
	@echo 'composer: composer install'
clean:
	@rm -rf app/cache/*
	@rm -rf vendor/*
build: composer assets
composer:
	@composer install
cc:
	@app/console cache:clear
cc-prod:
	@app/console cache:clear --env=prod
watch:
		@app/console assetic:watch
assets:
	@app/console assetic:dump
	@app/console assets:install
.get-composer:
	@curl -o composer.phar https://getcomposer.org/composer.phar
.make-swap:
	@/bin/dd if=/dev/zero of=/var/swap.1 bs=1M count=1024
	@/sbin/mkswap /var/swap.1
	@/sbin/swapon /var/swap.1
