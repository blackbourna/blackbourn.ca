<?php

namespace Blackbourn\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('BlackbournMainBundle:Default:index.html.twig');
    }

    public function resumeAction()
    {
        return $this->render('BlackbournMainBundle:Default:resume.html.twig');
    }

    public function amazebotAction()
    {
        return $this->render('BlackbournMainBundle:Default:amazebot.html.twig');
    }
}
